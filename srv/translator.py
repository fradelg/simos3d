"""
Translate a collada object defined by PyCollada into SceneJS JSON and
outputs the result to a stream (a file, string or network socket)
"""

import collada
import sys
import shutil
import os
import zipfile
from numpy import array, append, identity, array_equal

def newId(id):
    " Generate a new ID according to an accounting table of times used before "
    global lastId
    if not id in lastId:
        lastId[id] = 0
        return id
    else:
        lastId[id] += 1
        return id + '#' + str(lastId[id])

def convert3dmodel( tmpdir = "/tmp/", path = ".", filename = "" ):
    """ Convert the zipped DAE 3D model into a JSON scene graph

        :Parameters:
          tmpdir
            The temporal dir in which extracts the .dae model and its textures
          path
            The dir which contains the zipped file
          filename
            The name of the ZIP file
    """

    global filepath, textured, lastId
    filepath = tmpdir
    textured = {}
    lastId = {}

    # Unzip compressed data
    if filename.endswith(".zip"):
        zfile = zipfile.ZipFile(path + filename, "r")
        if not os.path.isdir(tmpdir):
            os.mkdir(tmpdir)
        zfile.extractall(tmpdir)

        for name in zfile.namelist():
            if name.endswith(".dae"):
                filename = name

    elif filename.endswith(".dae"):
        shutil.copy(path + filename, tmpdir + filename)

    # Check whether the collada file exists and try to load it into a collada object
    filename = tmpdir + filename
    if not os.path.isfile(filename):
        return "Error: '" + filename + "' is not a valid file path"

    collada_obj = collada.Collada(filename, ignore=[collada.DaeUnsupportedError])

    # Convert COLLADA model into a SceneJS graph
    return translate(collada_obj)

def translate(collada_obj, debug = False, verbose = False):
    """
      Translates a collada_obj given by PyCollada into SceneJS JSON and
      outputs the result to a stream.

      :Parameters:
        collada_obj
          Collada object given by PyCollada.
    """
    global _debug, _verbose
    _debug, _verbose = debug, verbose

    # Export libraries
    jslib = {
        'type': 'library',
        'id': 'library',
        'nodes': []
    }

    # Export materials
    for mat in collada_obj.materials:
        jsmat = translate_material(mat)
        if jsmat:
            jslib['nodes'].append(jsmat)

    # Export geometries
    for geom in collada_obj.geometries:
        jsgeom = translate_geometry(geom)
        if jsgeom:
            jslib['nodes'].append(jsgeom)

    # Export scenes
    for scene in [collada_obj.scene]:
        jsscene = translate_scene(scene)
        if jsscene:
            # Link the library node to the current scene (as a core library)
            jsscene['nodes'].insert(0, jslib)
            return jsscene

# Helpers
def _float_attribute(jsnode, key, val):
    if not val or type(val) is not float:
        return False
    jsnode[key] = val
    return True

def _rgb_attribute(jsnode, key, val):
    if not val or type(val) is not tuple:
        return False
    jsnode[key] = { 'r': val[0], 'g': val[1], 'b': val[2] }
    return True

def translate_material_map(material_map):
    jslayer = {}

    # Pre-conditions
    if not material_map.sampler: return None
    if not material_map.sampler.surface: return None
    if not material_map.sampler.surface.image: return None
    if not material_map.sampler.surface.image.path: return None

    global filepath
    if filepath.startswith("../"):
        filepath = filepath.replace("../", "")
    imagepath = str(material_map.sampler.surface.image.path)
    if imagepath.startswith("../"):
        imagepath = imagepath.replace("../", "")

    # Set attributes of layer node for texture node
    jslayer['uri'] = filepath + imagepath
    jslayer['wrapS'] = 'repeat'
    jslayer['wrapT'] = 'repeat'
    jslayer['flipY'] = 'true'
    jslayer['blendFactor'] = '0.5'

    if material_map.sampler.minfilter:
        filters = {
            'NONE': 'nearest',
            'NEAREST': 'nearest',
            'LINEAR': 'linear',
            'NEAREST_MIPMAP_NEAREST': 'nearestMipMapNearest',
            'LINEAR_MIPMAP_NEAREST': 'linearMipMapNearest',
            'NEAREST_MIPMAP_LINEAR': 'nearestMipMapLinear',
            'LINEAR_MIPMAP_LINEAR': 'linearMipMapLinear'
        }
        jslayer['minFilter'] = filters[material_map.sampler.minfilter.upper()]

    if material_map.sampler.magfilter:
        filters = {
            'NONE': 'nearest',
            'NEAREST': 'nearest',
            'LINEAR': 'linear',
            'NEAREST_MIPMAP_NEAREST': 'linear',
            'LINEAR_MIPMAP_NEAREST': 'linear',
            'NEAREST_MIPMAP_LINEAR': 'linear',
            'LINEAR_MIPMAP_LINEAR': 'linear'
        }
        jslayer['magFilter'] = filters[material_map.sampler.magfilter.upper()]

    return jslayer

def translate_material(mat):
    """
      Translates a collada material node into a SceneJS material node.

      :Parameters:
        geom
          An instance of the PyCollada Material class.
    """
    jstexture = {
        'type': 'texture',
        'coreId': 'texture-' + mat.id,
        'layers': []
    }
    jsmaterial = {
        'type': 'material',
        'coreId': mat.id
    }
    if not _rgb_attribute(jsmaterial, 'baseColor', mat.effect.diffuse):
        _rgb_attribute(jsmaterial, 'baseColor', (0.5, 0.5, 0.5))
        if type(mat.effect.diffuse) is collada.material.Map:
            # Create a texture layer for the diffuse map
            jslayer = translate_material_map(mat.effect.diffuse)
            if jslayer:
                jslayer['applyTo'] = 'baseColor'
                jstexture['layers'].append(jslayer)
        elif _verbose and mat.effect.diffuse:
            print "Unknown diffuse input: " + str(mat.effect.diffuse)
    if not _rgb_attribute(jsmaterial, 'specularColor', mat.effect.specular):
        if type(mat.effect.specular) is collada.material.Map:
            # Create a texture layer for the specular map
            jslayer = translate_material_map(mat.effect.specular)
            if jslayer:
                jslayer['applyTo'] = 'specular'
                jstexture['layers'].append(jslayer)
        elif _verbose and mat.effect.specular:
            print "Unknown specular input: " + str(mat.effect.specular)
    if not _float_attribute(jsmaterial, 'shine', mat.effect.shininess):
        if _verbose and mat.effect.shininess:
            print "Unknown shininess input: " + str(mat.effect.shininess)
    if not _float_attribute(jsmaterial, 'alpha', mat.effect.transparency):
        if type(mat.effect.transparency) is collada.material.Map:
            # Create a texture layer for the alpha map
            jslayer = translate_material_map(mat.effect.transparency)
            if jslayer:
                jslayer['applyTo'] = 'alpha'
                jstexture['layers'].append(jslayer)
        elif _verbose and mat.effect.transparency:
            print "Unknown alpha input: " + str(mat.effect.transparency)
    if mat.effect.emission and type(mat.effect.emission) is tuple:
        jsmaterial['emit'] = (mat.effect.emission[0] + mat.effect.emission[1] + mat.effect.emission[2]) / 3.0
    else:
        if type(mat.effect.emission) is collada.material.Map:
            # Create a texture layer for the emission map
            jslayer = translate_material_map(mat.effect.emission)
            if jslayer:
                jslayer['applyTo'] = 'emit'
                jstexture['layers'].append(jslayer)
        elif _verbose and mat.effect.emission:
            print "Unknown emit input: " + str(mat.effect.emission)

    # Add the material to the texture if suitable
    if jstexture['layers']:
        jsmaterial['nodes'] = [jstexture]
        textured[mat.id] = True
    else:
        textured[mat.id] = False

    return jsmaterial

def translate_geometry(geom):
    """
      Translates a collada geometry node into one or more SceneJS geometry nodes.

      :Parameters:
        geom
          An instance of the PyCollada Geometry class.
    """

    geomId = geom.id
    if geomId.endswith('-mesh'):
        geomId = geomId[:-5]

    jsgeom = {
        'type': 'geometry',
        'coreId': geomId,
        'positions': [],
        'normals': [],
        'uv': []
    }

    jssubgeoms = []
    g_count = 0
    v_count = 0
    indexMap = dict()

    for prim in geom.primitives:
        if type(prim) is collada.triangleset.TriangleSet \
              or type(prim) is collada.polylist.Polylist:
              #or type(prim) is collada.lineset.LineSet:

            jssubgeom = {
                'type': 'geometry',
                'coreId': geomId + '#' + str(g_count),
                'primitive': 'lines' if type(prim) is collada.lineset.LineSet else 'triangles',
                'indices': []
            }

            jssubgeoms.append(jssubgeom)

            g_count += 1

            if type(prim) is collada.polylist.Polylist:
                prim = prim.triangleset()

            if type(prim) is collada.triangleset.TriangleSet:
                nprim = prim.ntriangles
                nvertex = 3
            elif type(prim) is collada.lineset.LineSet:
                nprim = prim.nlines
                nvertex = 2

            for i in range(0, nprim):
                for v in range(0, nvertex):
                    # Create an identifier for the three different index coming from different sources
                    vid = 'v' + str(prim.vertex_index[i][v])
                    vid = vid + 'n' + str(prim.normal_index[i][v]) if prim.sources['NORMAL'] else vid
                    vid = vid + 't' + str(prim.texcoord_indexset[0][i][v]) if prim.sources['TEXCOORD'] else vid
                    if vid in indexMap:
                        jssubgeom['indices'].append(indexMap[vid])
                    else:
                        indexMap[vid] = v_count
                        jssubgeom['indices'].append(v_count)
                        v_count += 1

                        jsgeom['positions'].extend([float(p) for p in prim.vertex[prim.vertex_index][i][v]])
                        if prim.sources['NORMAL']:
                            jsgeom['normals'].extend([float(n) for n in prim.normal[prim.normal_index][i][v]])
                        else:
                            jsgeom['normals'].extend([0.0, 0.0, 0.0])
                        if prim.sources['TEXCOORD']:
                            jsgeom['uv'].extend([float(uv) for uv in prim.texcoordset[0][prim.texcoord_indexset[0]][i][v]])
                        else:
                            jsgeom['uv'].extend([0.0, 0.0])

        else:
            print "Warning: '" + type(prim).__name__ + "' geometry type is not yet supported by the translator."

    # Integrate all the different primitives into a parent geometry node with zero or more sub-geometries
    if len(jssubgeoms) == 0:
        print "Warning: No recognizable primitives found in Geometry '" + geomId + "'."
    elif len(jssubgeoms) == 1:
        jsgeom['primitive'] = jssubgeoms[0]['primitive']
        jsgeom['indices'] = jssubgeoms[0]['indices']
    else:
        jsgeom['nodes'] = jssubgeoms

    return jsgeom

def contains_light_nodes(jsnodes):
    # TODO: For now this function just checks if there's a child light node, it does not check all the way down the hierarchy
    #       If lights are nested deeply, then it's likely there would be an ordering problem anyway that couldn't be resolved
    #       with a simple reordering algorithm
    # Because lights are inserted first, it is only necessary to test whether the first node is a light
    return jsnodes and jsnodes[0]['type'] == 'light'

def _translate_scene_nodes(nodes):
    """
      Recursively translates collada scene graph nodes (instantiating the nodes defined in the library)

      :Parameters:
        node
          Any node in the collada visual scene not handled by the other methods.
    """
    global textured
    jsnodes = []
    for node in nodes:
        if type(node) is collada.scene.GeometryNode:
            geomId = node.geometry.id
            if geomId.endswith('-mesh'):
                geomId = geomId[:-5]

            jsgeometry = {
	              'type': 'name',
	              'name': geomId,
                'id': newId(geomId),
	              'nodes': [{
                    'type': 'geometry',
                    'coreId': geomId
	              }]
	          }

            if len(node.materials) > 1:
                jsgeometry['nodes'][0]['nodes'] = []

                # Iterate over the materials of this node
                for i in range(0, len(node.materials)):
                    subId = geomId + '#' +  str(i)
                    jsnode = {
                        'type': 'name',
                        'name': subId,
                        'id': newId(subId),
                        'nodes': [{
                            'type': 'geometry',
                            'coreId': subId
                        }]
                    }

                    # Create texture node if there exists
                    if textured[node.materials[i].target.id]:
                        jsnode = {
                            'type': 'texture',
                            'coreId': 'texture-' + node.materials[i].target.id,
                            'nodes': [ jsnode ]
                        }

                    jsgeometry['nodes'][0]['nodes'].append({
                        'type': 'material',
                        'coreId': node.materials[i].target.id,
                        'nodes': [ jsnode ]
                    })

            elif len(node.materials) == 1:
                # Create texture node if there exists
                if textured[node.materials[0].target.id]:
                    jsgeometry = {
                        'type': 'texture',
                        'coreId': 'texture-' + node.materials[0].target.id,
                        'nodes': [ jsgeometry ]
                    }

                jsgeometry = {
                    'type': 'material',
                    'coreId': node.materials[0].target.id,
                    'nodes': [ jsgeometry ]
                }

            else:
                # Add a default material to every geometry node
                jsgeometry = {
                    'type': 'material',
                    'nodes': [ jsgeometry ]
                }

            # Add Geometry node to main list
            jsnodes.append(jsgeometry)

        elif type(node) is collada.scene.ControllerNode:
            pass
        elif type(node) is collada.scene.CameraNode:
            # TODO: Cameras should be on top of the hierarchy in scenejs
            pass
        elif type(node) is collada.scene.LightNode:
            mode = None
            if type(node.light) is collada.light.AmbientLight or type(node.light) is collada.light.BoundAmbientLight:
                mode = 'ambient'
            elif type(node.light) is collada.light.DirectionalLight or type(node.light) is collada.light.BoundDirectionalLight:
                mode = 'dir'
            elif type(node.light) is collada.light.PointLight or type(node.light) is collada.light.BoundPointLight:
                mode = 'point'
            elif type(node.light) is collada.light.SpotLight or type(node.light) is collada.light.BoundSpotLight:
                mode = 'spot'

            if mode:
                jslight = {
                    'type': 'light',
                    'mode': mode,
                    'color': { 'r': node.light.color[0], 'g': node.light.color[1], 'b': node.light.color[2] }
                    #'diffuse': True # (default value)
                    #'specular': True # (default value)
                }
                if mode == 'point':
                    jslight['constantAttenuation'] = node.light.constant_att
                    jslight['linearAttenuation'] = node.light.linear_att
                    jslight['quadraticAttenuation'] = node.light.quad_att
                    #jslight['pos'] = { 'x': node.light.position[0], 'y': node.light.position[1], 'z': node.light.position[2] }
                    jslight['pos'] = {}
                    jslight['pos']['x'] = float(node.light.position[0])
                    jslight['pos']['y'] = float(node.light.position[1])
                    jslight['pos']['z'] = float(node.light.position[2])

                if mode == 'dir':
                    jslight['dir'] = {}
                    jslight['dir']['x'] = float(node.light.direction[0])
                    jslight['dir']['y'] = float(node.light.direction[1])
                    jslight['dir']['z'] = float(node.light.direction[2])

                # Light nodes should always be placed first in the list
                jsnodes.insert(0, jslight)

        elif type(node) is collada.scene.Node:
            # COLLADA matrices are transposed
            elems = [float(element) for row in node.matrix.transpose() for element in row]
            jsmatrix = { 'type': 'matrix', 'elements': elems, 'nodes': [] }
            jsnodes.append(jsmatrix)
            if node.children:
                jsmatrix['nodes'] = _translate_scene_nodes(node.children)

        elif type(node) is collada.scene.NodeNode:
            # <instance_node> references to one node in library_nodes
            # redirect to children of the node and concat with current jsnodes
            jsnodes += _translate_scene_nodes(node.node.children)

        else:
            print 'Node: ' + str(type(node))

    return jsnodes

def translate_camera(camera):
    """
      Translates a collada camera into SceneJS lookAt and camera nodes.
    """
    return {
        'id': 'main-lookAt',
        'type': 'lookAt',
        'eye': { 'x': float(camera.position[0]), 'y': float(camera.position[1]), 'z': float(camera.position[2]) },
        'look': { 'x': float(camera.position[0] + camera.direction[0]), 'y': float(camera.position[1] + camera.direction[1]), 'z': float(camera.position[2] + camera.direction[2]) },
        'up': { 'x': float(camera.up[0]), 'y': float(camera.up[1]), 'z': float(camera.up[2]) },
        'nodes': [{
            'id': 'main-camera',
            'type': 'camera',
            'optics': {
                 'type': 'perspective', #TODO: type of camera can't be retrieved a.t.m. assuming "perspective" for now
                 'fovy': float(camera.fov),
                 'aspect': 1.0, # TODO: aspect ratio is not currently available
                 'near': float(camera.near),
                 'far': float(camera.far)
            },
            'nodes': []
        }]
    }

# Bound coordinates of the bounding box
x0 = sys.float_info.max
y0 = sys.float_info.max
z0 = sys.float_info.max
x1 = -sys.float_info.max
y1 = -sys.float_info.max
z1 = -sys.float_info.max

def calculate_bounds(scene):
    """ Calculate bounding box with the objects in the scene """
    for geom in scene.objects('geometry'):
        for prim in geom.primitives():
            global x0, x1, y0, y1, z0, z1
            for vert in prim.vertex:
                x0 = min (x0, vert[0])
                x1 = max (x1, vert[0])
                y0 = min (y0, vert[1])
                y1 = max (y1, vert[1])
                z0 = min (z0, vert[2])
                z1 = max (z1, vert[2])

def translate_scene(scene):
    """
      Translates collada scene graph hierarchy into a SceneJS scene graph.
      This makes some changes to the hierarchy, such as placing camera nodes above visual nodes.
    """
    global x0, x1, y0, y1, z0, z1

    calculate_bounds(scene)

    scenenodes = _translate_scene_nodes(scene.nodes)

    zoom = max((x1-x0), max((y1-y0), (z1-z0)))

    jsflags = {
        'type': 'flags',
        'id': 'scene-flags',
        'flags': {
            'transparent': True,
            'backfaces': True
        },
        'nodes': []
    }

    jsrenderer = {
        'type': 'renderer',
        'clear': { 'depth': True, 'color': True, 'stencil': False },
        'clearColor': { 'r': 0.6, 'g': 0.6, 'b': 0.6 },
        'nodes': []
    }

    jsscale = {
        'type': 'scale',
        'x': 1 / zoom,
        'y': 1 / zoom,
        'z': 1 / zoom,
        'nodes': []
    }

    jstranslate = {
        'type': 'translate',
        'id': 'scene-origin',
#        'x': -(x0+x1) / 2,
#        'y': -(y0+y1) / 2,
#        'z': -(z0+z1) / 2,
        'nodes': []
    }

    jscamera = {
        'type': 'lookAt',
        'id': 'main-lookAt',
        'eye': { 'x': 0.0, 'y': 0.2, 'z': 1.0 },
        'look': { 'x': 0.0, 'y': 0.2, 'z': 0.0 },
        'up': { 'x': 0.0, 'y': 1.0, 'z': 0.0 },
        'nodes': [{
            'type': 'camera',
            'id': 'main-camera',
            'optics': {
                 'type': 'perspective',
                 'fovy': 60.0,
                 'aspect': 1.0,
                 'near': 0.1,
                 'far': 10000.0
            },
            'nodes': []
        }]
    }

    jsflags['nodes'] = scenenodes
    jstranslate['nodes'] = [jsflags]
    jsscale['nodes'] = [jstranslate]
    jsrenderer['nodes'] = [jsscale]
    jscamera['nodes'][0]['nodes'] = [jsrenderer]

    return {
        'type': 'scene',
        'id': 'Scene',
        'canvasId': 'scenejsCanvas',
        'nodes': [ jscamera ]
    }
