import java.util.List;
import py4j.GatewayServer;
import net.sf.mpxj.mpp.MPPReader;
import net.sf.mpxj.reader.ProjectReader;
import net.sf.mpxj.ProjectFile;
import net.sf.mpxj.Task;
import net.sf.mpxj.MPXJException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class MppEntryPoint {

    private static GatewayServer gatewayServer;

    public MppEntryPoint() 
    {
    }

    public static String read(String filename) throws MPXJException
    {
        ProjectReader reader = new MPPReader();
        ProjectFile project = reader.read(filename);
        // The first task is the filename itself so we start from its child
        Task task = project.getChildTasks().get(0);
        JSONArray results = listHierarchy(task.getChildTasks(), "");
        return results.toString();
    }

    private static JSONArray listHierarchy(List<Task> tasks, String prefix)
    {
        JSONArray children = new JSONArray();
        int count = 0;
        for (Task task : tasks)
        {
            JSONObject object = new JSONObject();
            String label = prefix + count + " - " + task.getName();
            object.put("label", label);
            JSONArray subchildren = listHierarchy(task.getChildTasks(), prefix + count + '.');
            if (subchildren.size() != 0)
                object.put("children", subchildren);
            children.add(object);
            count++;
        }
        return children;
    }

    public static void stop()
    {
        gatewayServer.shutdown();
        System.out.println("Gateway Server Stopped");
    }

    public static void main(String[] args) 
    {
        gatewayServer = new GatewayServer(new MppEntryPoint());
        gatewayServer.start();
        System.out.println("Gateway Server Started");
//        try {
//            MppEntryPoint.read("example.mpp");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}