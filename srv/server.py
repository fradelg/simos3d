#!/usr/bin/env python
import os, time, sys
import shutil
import subprocess
import json
import bottle
import smtplib
import hashlib
import random
import string

from email.mime.text import MIMEText
from translator import convert3dmodel
from py4j.java_gateway import JavaGateway
from bottle import route, run, static_file, request, install
from bottle.ext import sqlite

config_file = open( 'config.json' )
config_data = json.load( config_file )
path_db     = config_data["paths"]["database"]
path_3d     = config_data["paths"]["3d"]
path_tex    = config_data["paths"]["textures"]
path_upload = config_data["paths"]["uploads"]
path_tmp    = config_data["paths"]["tmp"]

# Load the SQLite bottle plugin
install( sqlite.Plugin( dbfile = path_db ) )

@route('/login/<email>/<password>' , method='GET')
def login(email, password, db):
    row = db.execute('SELECT * from user where email="' + email + '" and password="' + password + '"').fetchone()
    if row:
        return { 'success': True }
    return HTTPError(404, "User not found")

@route('/register/<name>/<email>/<password>' , method='GET')
def register(name, email, password, db):
    row = db.execute('SELECT name FROM user WHERE email="' + email + '"').fetchone()
    if row:
        return HTTPError(404, "The e-mail has been already registered in the system")
    db.execute('INSERT INTO user VALUES ("' + name + '","' + email + '","' + password + '")')
    return { 'success': True }

@route('/remember/<email>' , method='GET')
def remember(email, db):
    row = db.execute('SELECT name FROM user WHERE email="' + email + '"').fetchone()
    if row:
        sender = 'simos3d@avertia.com'
        receivers = [ email ]
        password = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(8))
        db.execute('UPDATE user SET password="' + hashlib.md5(password).hexdigest() + '" WHERE email="' + email + '"')
        msg = "From: SIMOS 3D support team <" + sender + ">\n"
        msg = msg + "To: " + row['name'] + " <" + email + ">\n"
        msg = msg + "Subject: Remember your password\n\n"
        msg = msg + "Your new password is " + password + "\n"
        smtpObj = smtplib.SMTP('localhost', 25)
        smtpObj.sendmail(sender, receivers, msg)
        smtpObj.quit()
        return { 'success': True }
    return HTTPError(404, "There is no user registered in the system with this e-mail")

@route('/bimmodel')
def bimmodel_list(db):
    bimlist = []
    email = request.query.email
    rows = db.execute('SELECT * from bimmodel where email="' + email + '"').fetchall()
    for entry in rows:
        bimmodel = {}
        bimmodel['filename'] = entry['name']
        bimmodel['description'] = entry['description']
        bimmodel['size'] = format( os.path.getsize( path_3d + entry['name'] + '.json' ) / 1024, ',d') + ' KB'
        bimmodel['date'] = time.ctime(os.path.getmtime( path_3d + entry['name'] + '.json' ))
        bimlist.append( bimmodel )
    return { 'result' : bimlist }

@route('/bimmodel/<name>', method='GET')
def bimmodel_get( name = "" ):
    if "" != name:
        model_file = open( os.path.join( path_3d, name + '.json') )
        return { 'data': json.load( model_file ) }
    else:
        return HTTPError( 404, 'BIM model not found: ' + name )

@route('/bimmodel/<name>', method='DELETE')
def bimmodel_delete( name, db ):
    if "" != name:
        email = request.forms.get( 'email' )
        # Remove database entry for this model
        db.execute('DELETE FROM bimmodel WHERE name="' + name + '" AND email="' + email + '"')
        # Remove the 3d model in JSON
        os.remove( os.path.join( path_3d, name + '.json' ) )
        # Remove the texture files attached to the model name
        pathname = os.path.abspath( path_tex )
        for filename in os.listdir( pathname ):
            if filename.startswith( name ):
                os.remove( os.path.join( pathname, filename ) )
        return { 'success' : True  }

@route('/bimmodel/<name>', method='POST')
def bimmodel_save( name, db ):
    # Recover params of the POST request
    data = request.forms.get( 'plan' )
    email = request.forms.get( 'email' )
    newname = request.forms.get( 'newname' )
    description = request.forms.get( 'description' )
    updated = request.forms.get( 'updated' )

    if "" != name and "" != newname and "" != data and "" != description:
        path = path_3d if updated == 'true' else path_tmp
        model_file = open( os.path.join( path, name + '.json' ) )
        model = json.load( model_file )
        os.remove( os.path.join( path, name + '.json' ) )

        # Append schedule and layer binding information to the 3d model
        if updated == 'false':
            if 'data' in model:
                model['data']['plan'] = data
            else:
                model['data'] = { 'plan': data }

        # Move the texture images to the textures directory with the 3d model
        if not os.path.exists( path_tex ):
            os.makedirs( path_tex )

        ntexture = 0
        for material in model['nodes'][0]['nodes']:
            if 'nodes' in material:
                if 'layers' in material['nodes'][0]:
                    for layer in material['nodes'][0]['layers']:
                        filename, extension = os.path.splitext( layer['uri'] )
                        filename = os.path.join( path_tex, newname + str(ntexture) + extension )
                        if os.path.exists( '../' + layer['uri'] ):
                            shutil.move( '../' + layer['uri'], filename )
                        ntexture =  ntexture + 1
                        if filename.startswith("../"):
                            filename = filename.replace("../", "")
                        layer['uri'] = filename

        # Save the file to the 3d models path
        with open( os.path.join( path_3d, newname + ".json" ), "w" ) as outfile:
            json.dump( model, outfile )

        # Insert the new bim model data into the database
        if updated == 'true':
            db.execute('UPDATE bimmodel SET name="' + newname + '", description="' + description +
                '" WHERE name="' + name + '" AND email="' + email + '"')
        else:
            db.execute('DELETE FROM bimmodel WHERE name="' + newname + '" AND email="' + email + '"')
            db.execute('INSERT INTO bimmodel VALUES ("' + newname + '","' + description + '","' + email + '")')
        return { 'success' : True, 'message' : 'Planning has been successfully saved' }
    else:
        return HTTPError( 404, 'Error: model name or binding information not found' )

@route('/planification/<name>', method='GET')
def planification_get( name = "" ):
    if "" != name:
        gateway = JavaGateway()
        plan = gateway.jvm.MppEntryPoint.read(path_upload + name)
        return { 'data': plan }
    else:
        return HTTPError( 404, 'The planification has not been uploaded yet' )

@route('/model3d/<name>', method='GET')
def model3d_get( name="" ):
    if "" != name:
        # Look for a copy in the uploads cache
        filename = os.path.join( path_tmp, name + ".json" )
#        if os.path.exists( filename ):
#            return json.load( open( filename ) )
        model = convert3dmodel( path_tmp, path_upload, name )
        with open( filename, 'w') as outfile:
            json.dump( model, outfile )
        return model
    else:
        return HTTPError( 404, 'The 3D model has not been uploaded' )

# Start in the background the py4j gateway which imports MPP files
subprocess.Popen(["java", "-cp", ".:json-simple.jar:mpxj.jar:py4j.jar:poi.jar", "MppEntryPoint"])

# Run the REST web server as a standalone application
run( host = config_data['host'], port = config_data['port'], debug = True )
