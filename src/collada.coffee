# Efficient search of nodes labelled with 'name'
filterNode = (data, name) ->
  (($ data).find '*').filter () -> this.nodeName == name

# Loads a basic scenejs scene graph serialized in json from the client
loadSceneJsModel = () ->
  reader = new FileReader()
  reader.onloadend = (file) ->
    try
      loadScene $.parseJSON f.target.result
    catch error
      console?.log error

  file = ($ '#upload-file').get(0)?.files[0]
  if file?
    reader.readAsText file
  else
    console?.log "No file selected"

###
  Load the scene of a 3D model into SceneJS engine
###
loadColladaScene = (scene) ->
  cleanCanvas()
  try
    SceneJS.createScene scene
    state.scene = SceneJS.scene 'Scene'
    viewportInit()
    sceneInit()
    state.scene.start
      idleFunc: SceneJS.FX.idle
      frameFunc: (params) ->
        $("#fps-display").html("FPS: " + params.fps)
  catch error
    console.log error
    
###
  Sets the GPS localization of the model
###
setGlobalPosition = (event) ->
  ($ "#gps-dialog").dialog 'close'
  state.gps.latitude  = $("#latitude-box").attr 'value'
  state.gps.longitude = $("#longitude-box").attr 'value'
  state.gps.altitude  = $("#altitude-box").attr 'value'
  calcLengthDegree state.gps.latitude

###
  Compute the length of one degree in longitude and latitude
###
calcLengthDegree = (latitude) ->
  state.gps.earthradius = 6367449
  state.gps.latlen = Math.PI * state.gps.earthradius / 180.0
  state.gps.lonlen = Math.PI / 180.0 * state.gps.earthradius * Math.cos (Math.PI / 180.0 * latitude)
  state.gps.latlen /= state.gps.unitmeter
  state.gps.lonlen /= state.gps.unitmeter
