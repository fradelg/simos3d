# Check whether enter key has been pressed
disableEnterKey = (event) ->
 key = if window.event? then window.event.keyCode else event.keyCode
 key isnt 13
 
# Register document events
registerDOMEvents = () ->
  ($ state.viewport.domElement).mousedown mouseDown
  ($ state.viewport.domElement).on 'touchstart', mouseDown
  ($ state.viewport.domElement).mouseup mouseUp
  ($ state.viewport.domElement).on 'touchend', mouseUp
  ($ state.viewport.domElement).mousemove mouseMove
  ($ state.viewport.domElement).on 'touchmove', mouseMove
  ($ state.viewport.domElement).mousewheel mouseWheel
  
#  document.addEventListener 'keydown', keyDown, true
  window.addEventListener 'resize', windowResize, true

# Register UI controls events
registerControlEvents = () ->
  ($ '#bim-models').delegate 'tr', 'click', selectBimModel

  ($ '#new-bim-model').click showUploadDialog
  ($ '#open-bim-model').click loadBimModel
  ($ '#save-bim-model').click () -> ($ '#dialog-save').dialog 'open'
  ($ '#play-simulation').click playAnimation
  ($ '#preferences-button').click () -> ($ '#dialog-preferences').dialog 'open'
  
  ($ '#login-button').click showLogin
  ($ '#create-user').button().click () -> $( '#dialog-newuser' ).dialog 'open'
  ($ '#forgotpass').button().click () -> $( '#dialog-remember' ).dialog 'open'

  ($ '#controls-relationships').delegate '.controls-tree-item', 'click', controlsToggleTreeOpen
  ($ '#controls-relationships').delegate '.controls-tree-item', 'dblclick', controlsShowProperties
  ($ '#controls-relationships').delegate 'input', 'change', controlsToggleTreeVisibility
  ($ '#controls-layers').delegate 'input', 'change', controlsToggleLayer

  ($ state.viewport.domElement).dblclick controlsShowProperties
