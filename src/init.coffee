###
  Destroy the current Scene
###
cleanCanvas = () ->
  if state.scene?
    gl = state.scene.get 'glContext'
    state.scene.destroy()
    state.scene = null
    state.selectedItem = null
    gl.clearColor 0, 0, 0, 0
    gl.clear gl.COLOR_BUFFER_BIT

###
  Initialize the viewport div in the HTML document
###
viewportInit = () ->
  $('#scenejsCanvas').toggleClass 'simod3d-watermark', not state.scene?

###
  Initialize the scene graph in the SceneJS WebGL engine
###
sceneInit = () ->
  # Set the correct aspect ratio according to the viewport size
  camera = state.scene.findNode 'main-camera'
  modifySubAttr camera, 'optics', 'aspect', state.canvas.width / state.canvas.height

  # Initialize the tag mask (for layers / hidden objects)
  state.scene.set 'tagMask', '^()$'

  # Store information about the lookAt node
  lookAt = state.scene.findNode 'main-lookAt'
  constants.lookAt =
    eye: lookAt.get 'eye'
    look: lookAt.get 'look'
    up: lookAt.get 'up'
  
  # Create transformation nodes for interactive manipulation of the scene
  camera.insert "node",
    type: "quaternion"
    id: "rotation"
    x: 1.0
    y: 0.0
    z: 0.0
    angle: -90.0

  camera.insert "node",
    type: "scale"
    id: "zoom-scale"
    x: 1.0
    y: 1.0
    z: 1.0

  # Add a global light for scene lighting
  camera.insert "node", constants.sceneLight
  
  # Add a special material for highlighting objects to the material library
  (state.scene.node 0).add "node", constants.highlightMaterial

###
  Create and initialize form/html controls related to the loaded scene
###
controlsInit = () ->
  # Populate the layers tab
  html = ''
#  for ifcType in sceneData.ifcTypes
#    html += "<div><input id='layer-" + ifcType
#    html += "' type='checkbox' checked='checked'> " + ifcType + "</div>"
  ($ '#controls-layers').html html

  # Populate the objects tab
  ($ '#project-list').html parseStages state.plan

  # Initialize the accordion control
  ($ '#controls-accordion').accordion 
    header: 'h3'
    heightStyle: 'content'

  # Show the accordion control
  ($ '#main-view-controls').show()

  # Update menus when model is loaded
  updateMenu 'model-loaded'

# Create and initialize the objects tree
ifcProject = (obj) ->
  "<li class='controls-tree-root' id='" + obj.id +
  "'><div class='controls-tree-item'>" + obj.name + "</div></li>"

ifcRelationships = (type, rel) ->
  if rel? and rel.length > 0
    html = "<ul class='controls-tree'>"
    html += "<div class='controls-tree-heading'><hr><h4>" + type + "</h4></div>"
    for obj in rel
      html += ifcObjectDescription obj
    html += "</ul>"
  else
    ""

ifcObjectDescription = (obj) ->
  parents = ($ '#' + state.selectedItem).parents 'ul.controls-tree'
  indent = Math.min(parents.length, 6)
  state.selectedItem = obj.id
  html = "<li class='controls-tree-rel' id='" + 
    obj.id + "'><div class='controls-tree-item'>" + 
    "<span class='indent-" + String(indent) + "'/>" + 
    "<input type='checkbox' checked='checked'> " + obj.name + 
    "<span class='controls-tree-postfix'>(" + obj.type + ")</span></div>"
  html += ifcRelationships 'Composed By', obj.children
  html += "</li>"

###
  Generate a HTML tree with checkboxes from children
###
parseStages = (children, indent = 0) ->
  html = ''
  return if not children? 
  for child in children
    layer = null
    for item in state.binding 
      child.layer = item.layer.text() if item.stage.text() == child.label
    html += '<li class="controls-tree-rel"'
    html += 'id="' + child.layer + '"' if child.layer?
    html += '><div class="controls-tree-item">'
    html += '<span class="indent-' + String(indent) + '"/>'
    html += '<input type="checkbox" checked="checked">' + child.label + '</div>'
    if child.children? and child.children.length > 0
      html += '<ul class="controls-tree">' + (parseStages child.children, indent + 1) + '</ul>'
    html += '</li>'
  return html