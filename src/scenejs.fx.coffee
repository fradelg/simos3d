# Additional math routines not found in standard JavaScript or SceneJS
Math.clamp = (s, min, max) -> Math.min(Math.max(s, min), max)

# Conversion of various scenejs types / structures
recordToVec3 = (record) ->
  return [record.x, record.y, record.z]

recordToVec4 = (record) ->
  return [record.x, record.y, record.z, record.w]

vec3ToRecord = (vec) ->
  return { x: vec[0], y: vec[1], z: vec[2] }

vec4ToRecord = (vec) ->
  return { x: vec[0], y: vec[1], z: vec[2], w: vec[3] }

lookAtToQuaternion = (lookAt) ->
  # Create an orthonormal basis
  eye = recordToVec3 lookAt.eye
  look = recordToVec3 lookAt.look
  up  = recordToVec3 lookAt.up
  x = [0.0, 0.0, 0.0]
  y = [0.0, 0.0, 0.0] 
  z = [0.0, 0.0, 0.0]
  SceneJS_math_subVec3 look, eye, z
  SceneJS_math_cross3Vec3 up, z, x
  SceneJS_math_cross3Vec3 z, x, y
  SceneJS_math_normalizeVec3 x
  SceneJS_math_normalizeVec3 y
  SceneJS_math_normalizeVec3 z

  # Convert to quaternion
  return SceneJS_math_newQuaternionFromMat3 x.concat y, z

# Initialize namespace
SceneJS.FX = {}

SceneJS.FX.idle = () ->
  SceneJS.FX.TweenSpline.update()

# Additional routines for modifying/manipulating scenejs node attributes
modifySubAttr = (node, attr, subAttr, value) ->
  attrRecord = node.get attr
  attrRecord[subAttr] = value
  node.set attr, attrRecord

lerpLookAt = (t, lookAt0, lookAt1) ->
  # Do spherical linear interpolation between quaternions to calculate the up vector
  q0 = lookAtToQuaternion lookAt0
  q1 = lookAtToQuaternion lookAt1
  q = SceneJS_math_slerp t, q0, q1

  result = 
    eye:  SceneJS_math_lerpVec3 t, 0.0, 1.0, lookAt0.eye, lookAt1.eye
    look: SceneJS_math_lerpVec3 t, 0.0, 1.0, lookAt0.look, lookAt1.look
    up:   vec3ToRecord SceneJS_math_newUpVec3FromQuaternion q

lerpLookAtNode = (node, t, lookAt0, lookAt1) ->
  node.set (lerpLookAt t, lookAt0, lookAt1)

### 
  Tween effect along a spline path
  USAGE:
    SceneJS.FX.TweenSpline node [, interval]
  TODO: 
    Implement the spline, for now we're just doing linear interpolation
###
SceneJS.FX.TweenSpline = do () ->
  class TweenSpline
    constructor: (lookAtNode, play) ->
      @_target = lookAtNode
      @_sequence = []
      @_timeline = []
      @_play = play ? true
      @_t = 0.0
    
    tick: (dt) ->
      @_t += dt if @_play
    
    start: (lookAt) ->
      @_sequence = [lookAt ? {
        eye: @_target.get 'eye'
        look: @_target.get 'look'
        up: @_target.get 'up' }]
      @_timeline = [0.0]
      @_t = 0.0

    push: (lookAt, dt) ->
      @_t = 0.0 if @_sequence.length == 0
      dt_prime = dt ? 5000
      if @_timeline.length == 0
        dt_prime = 0.0
      @_timeline.push @totalTime() + dt_prime
      @_sequence.push lookAt
    
    sequence: (lookAts, dt) ->
      @_t = 0.0 if @_sequence.length == 0
      for lookAt in lookAts
        dt_prime = if @_timeline.length > 0 then (dt ? 5000) else 0.0
        @_timeline.push @totalTime() + dt_prime
        @_sequence.push lookAt 
      return null
    
    pause: () -> @_play = false

    play: () -> @_play = true
    
    totalTime: () ->
      # CoffeeScript bug:
      # return @_timeline[@_timeline.length] if @_timeline.length > 0 else 0
      if @_timeline.length > 0
        return @_timeline[@_timeline.length - 1]
      return 0

    update: () ->
      # Remove from tweens if the sequence is empty
      return false if @_sequence.length == 0
      
      # Check if the animation is paused
      return true if not @_play

      # Remove from tweens if the sequence is complete
      if @_t >= @totalTime() || @_sequence.length == 1
        @_target.set @_sequence[@_sequence.length - 1]
        return false
      
      # Perform interpolation
      i = 0
      ++i while @_timeline[i] <= @_t
      dt = @_timeline[i] - @_timeline[i - 1]
      lerpLookAtNode @_target,
        (@_t - @_timeline[i - 1]) / dt,
        @_sequence[i - 1],
        @_sequence[i]
      return true
  
  _tweens = []
  _intervalID = null
  _dt = 0
  
  _tick = () ->
    for tween in _tweens
      tween.tick _dt
    null
  
  _r = (lookAtNode, interval) ->
    _dt = interval || 50       # The default interval is 50 ms equivalent to 20 FPS
    if _intervalID != null
      clearInterval _intervalID
    _intervalID = setInterval _tick, _dt
    tween = new TweenSpline lookAtNode
    _tweens.push tween
    return tween
  
  _r.update = () ->
    i = 0
    while i < _tweens.length
      tween = _tweens[i]
      if not tween.update()
        _tweens.splice i, 1
      else
        i += 1
  
  return _r
