### 
A utility to escape special regular expression characters from a string
  See http://www.regular-expressions.info/charclass.html for details
  Used http://www.javascriptkit.com/jsref/regexp.shtml for a reference list of special characters
  See also http://stackoverflow.com/questions/2593637/how-to-escape-regular-expression-in-javascript
###
RegExp.escape = (str) -> str.replace /[[\]\\$().{},?*+|^-]/g, "\\$&" if str?

### 
Display properties of the citygml object with the given id
###
controlsPropertiesSelectObject = (id) ->
  # Hide elements and check if id is a valid identifier
#  ($ '#controls-properties .controls-message').hide()
#  ($ '#controls-properties .property-table').hide()
#  return ($ '#controls-properties .controls-message').show() if not state.cityObjects[id]?
  
#  hiddenKeys = ['id','abstract', 'children', 'type', 'geometry', 'properties']

  object2Html = (keyStack, key, value) ->
    html = "<a class='ifc-link' href='#"
    if value.link? and typeof value.link == 'string'
      html += value.link + "'>#{value.link}</a>"
    else
      html += k + "/" for k in keyStack
      html += key + "'>...</a>"
  
  value2html = (key, value) ->
    if key is 'id'
      html = value
    else if (key.indexOf 'start') == 0 or (key.indexOf 'end') == 0 or (key.indexOf 'finish') == 0
      html = '<input type="text" value="' + value + '" class="date"/>'
    else if key in [ 'class', 'function', 'usage' ]
      type = state.cityObjects[id].type
      html = '<select multiple="multiple" class="selection">'
      for name, description of state.codelist['building_' + type + '_' + key]
        html += '<option value="' + name 
        html += '" selected="selected" ' if value is name
        html += '">' + description + '</option>'
      html += '</select>'
    else
      html = '<input type="text" value="' + value + '"/>'
    return html

  tableItem = (key, value) -> 
    # Discard object functions, hidden keys and xml schemas
    return '' if typeof value is 'function' or key in hiddenKeys or (key.charAt 0) is 'x'
    
    # Create a new table element
    html = "<tr><td class='label'>#{key}</td><td class='value'>"
    
    if Array.isArray value
      for i in [0..value.length-1]
        html += "<br>" if i > 0
        html += value2html key, value[i]
    else if typeof value is 'object'
#     html += object2html keyStack, key, value
      return ''
    else
      html += value2html key, value

    html += "</td></tr>"

  # Load properties for the given id
  if state.scene.data().properties? 
    properties = state.scene.data().properties[id]
  else
    properties = state.cityObjects[id]
  
  # Show the new property table
  element = $ ('#' + id + '-property')
  if element.length isnt 0
    return element.show()

  # Generate HTML code for an editable table of properties
  html = '<table id="' + id + '-property" class="property-table">'
  for key, value of properties
    html += tableItem key, value
  html += '</table>'
    
  # Add HTML code to accordion property tab
  ($ '#controls-properties').append html
  
  # Attach jQuery UI elements
  ($ '#' + id + '-property .date').datepicker()
  ($ '#' + id + '-property .selection').multiselect
    noneSelectedText: 'Select' 
    selectedList: 2
    header: false
    multiple: false
    maxWidth: 150
    click: (event, ui) ->
      selector = $ event.target
      label = (selector.closest 'td.value').siblings 'td.label'
      state.cityObjects[state.selectedItem][label.html()] = [ ui.value ]
  
  # Set callback for editing all entries
  ($ '#' + id + '-property :input').keypress (event) ->
    keyCode = event.keyCode || event.which
    if keyCode == 9 or keyCode == 13
      selector = $ event.target
      label = selector.parent().siblings 'td.label'
      state.cityObjects[state.selectedItem][label.html()] = [selector.attr 'value']

###
Toggle visibility of an object based on the state of its checkbox in the tree
###
controlsToggleTreeVisibility = (event) ->
  # Look for the closest <li> element in DOM tree
  $parent = ($ event.target).closest '.controls-tree-rel'
  parentId = $parent.attr 'id'

  # Propagate the event over the checkboxes of <li> children elements
  ($parent.find '.controls-tree-rel').each () -> 
    (($ this).find 'input:checkbox').prop 'checked', event.target.checked
    (($ this).find 'input:checkbox').trigger 'change'

  # Check whether this element is attached to a scene node
  return if not parentId?

  # Remove flags nodes if the checkbox has been ticked
  if event.target.checked
    nodes = state.scene.findNodes '^disable-' + (RegExp.escape parentId) + '$'
    for node in nodes
      node.splice() if node? 
    return

  # Create a new flags node to disable rendering of a tree branch
  flagsJson = 
    type: 'flags'
    id: 'disable-' + parentId
    flags:
      enabled: false

  # Reconnect the subtree to the new flagJson node
  node = state.scene.findNode parentId
  parentNode = node.parent()
  parentNode.add 'node', flagsJson if not parentNode.node flagsJson.id?
  (parentNode.node flagsJson.id).add 'node', node.disconnect()

###
  Select an object in the left tree and highlight its geometry in the scene graph
###
controlsTreeSelectObject = (id) ->
  return if not id?

  # Update the item selection into the tree structure
  controlsSelectTreeElement id

  # Populate tree with the element properties
  # controlsPropertiesSelectObject id
  
  # Delete any previously selected SceneJS node
  unHighlightNodes state.scene.findNode state.selectedItem if state.selectedItem?

  # Update the selectedItem reference
  if state.selectedItem is id
    state.selectedItem = null
    return
  
  # Highlight the material of the new selected node
  highlightNode state.scene.findNode id 
  state.selectedItem = id
  console.log? state.selectedNodes if state.debug
      
### 
  Remove avery ocurrence of highlighted material node
###
unHighlightNodes = (node) ->
  node.eachNode () ->
    (@parent().node 0).splice() if (@get 'coreId') is constants.highlightMaterial.coreId
  , depthFirst: true

### 
  Add highlight material above every geometry node of the selected scenejs node
###
highlightNode = (node) ->
  node.insert 'node',
    type: constants.highlightMaterial.type
    coreId: constants.highlightMaterial.coreId 

# Open up the properties tab of the controls accordion
controlsShowProperties = (event) ->
  return if event? and event.target.nodeName is 'INPUT'
  ($ '#controls-accordion').accordion 'activate', 1

# Navigate the selected link (display its properties)
controlsNavigateLink = (event) ->
  controlsPropertiesSelectObject (($ event.target).attr 'href').slice 1
  return false

###
  Toggle the visibility of a layer based on the state of its checkbox in the layers tab
###
controlsToggleLayer = () ->
  elements = ($ '#controls-layers input:checked').toArray()
  tags = (((($ el).attr 'id').split /^layer\-/)[1] for el in elements)
  state.scene.set 'tagMask', '^(' + (tags.join '|') + ')$'

### 
  Toggle a task in the schedule to open/closed state
###
controlsToggleTreeOpen = (event) ->
  $parent = ($ event.target).parent()
  $parent.toggleClass 'controls-tree-open'
  controlsTreeSelectObject $parent.attr 'id'

### 
  Select the activity linked to the geometry node id on the 3D model
###
controlsSelectTreeElement = (id) ->
  ($ '.controls-tree-selected').removeClass 'controls-tree-selected'
  ($ '.controls-tree-selected-parent').removeClass 'controls-tree-selected-parent'
  if state.selectedItem isnt id
    (($ '#' + id).children '.controls-tree-item').addClass 'controls-tree-selected'
    ($ '.controls-tree:has(.controls-tree-selected)').addClass 'controls-tree-selected-parent'

    # Toogle the parent item only if changed
    if (($ '#' + id).parents 'li') isnt state.parentActivity
        state.parentActivity.removeClass 'controls-tree-open' if state.parentActivity?
        state.parentActivity = ($ '#' + id).parents 'li'
        state.parentActivity.addClass 'controls-tree-open'

###
  Start to play the animation
###
playAnimation = (event) ->
  ids = []
  (($ '#project-list').find 'li:not(:has(ul))').each () ->
    id = ($ this).attr 'id'
    ids.push id if id?

  # Add flags node and alpha value to the material for each scenejs node
  state.materials = []
  for id in ids
    node = state.scene.findNode id

    # Search the first children corresponding to a material node
    material = null
    node.eachParent () ->
      material = @ if (@get 'type') is 'material'

    if material?
      # Save the material nodes in the same order as the tree
      state.materials.push 
        geometry: id
        material: material
        alpha: material.get 'alpha'

      # Initialize the alpha attribute of the material
      material.set 'alpha', 0.0

  # Create the timer which updates the alpha value every 40 milliseconds
  state.current = 0
  material = state.materials[state.current].material
  geometry = state.materials[state.current].geometry
  controlsSelectTreeElement geometry

  # Start timer to update the selected tree element and material alpha values
  state.timer = $.timer 1000 / constants.fps, () ->
    material.set 'alpha', (material.get 'alpha') + 1.0 / 50.0
    if (material.get 'alpha') > state.materials[state.current].alpha
      state.current++
      if state.current is state.materials.length
          ($ '#play-simulation a').text 'Simulate'
          ($ '#play-simulation').off().on 'click', playAnimation
          return state.timer.stop()
      material = state.materials[state.current].material
      geometry = state.materials[state.current].geometry

      # Select the new tree item for the current geometry node
      controlsSelectTreeElement geometry
  
  # Reset camera parameters to default values
  lookAt = state.scene.findNode 'main-lookAt'
  lookAt.set 'eye', constants.lookAt.eye
  lookAt.set 'look', constants.lookAt.look
  lookAt.set 'up', constants.lookAt.up
  rotation = state.scene.findNode 'rotation'
  rotation.set 'rotation',
    x: 1
    y: 0
    z: 0
    angle: -90.0
  zoom = state.scene.findNode 'zoom-scale'
  zoom.set 'x', 1.0
  zoom.set 'y', 1.0
  zoom.set 'z', 1.0

  # Define a camera path to rotate around the center of the scene
  state.snapshots.lookAts = []
  state.snapshots.lookAts.push
    eye: lookAt.get 'eye'
    look: lookAt.get 'look'
    up: lookAt.get 'up'
  state.snapshots.lookAts.push
    eye: { x: 1.0, y: 0.2, z: 0.0 }
    look: { x: 0.0, y: 0.2, z: 0.0 }
    up: { x: 0.0, y: 1.0, z: 0.0 }
  state.snapshots.lookAts.push
    eye: { x: 0.0, y: 0.2, z: -1.0 }
    look: { x: 0.0, y: 0.2, z: 0.0 }
    up: { x: 0.0, y: 1.0, z: 0.0 }
  state.snapshots.lookAts.push
    eye: { x: -1.0, y: 0.2, z: 0.0 }
    look: { x: 0.0, y: 0.2, z: 0.0 }
    up: { x: 0.0, y: 1.0, z: 0.0 }
  state.snapshots.lookAts.push
    eye: lookAt.get 'eye'
    look: lookAt.get 'look'
    up: lookAt.get 'up'

  # Compute total length of lerp animation
  length = 0.0
  length += (mat.alpha * 50.0 / constants.fps) for mat in state.materials
  length = length * 225.0
  
  # Perform slerp interpolation between camera path
  state.fx = SceneJS.FX.TweenSpline lookAt, constants.fps
  state.fx.sequence state.snapshots.lookAts, length

  # Tooggle Play/Stop control at top menu bar
  ($ '#play-simulation a').text 'Stop'
  ($ '#play-simulation').off().on 'click', ()->
    # Stop animations
    state.fx.pause()
    state.timer.stop()
    # Recover the original alpha value for each material
    for i in [0..state.materials.length-1]
      if state.materials[i].alpha > 0
        state.materials[i].material.set 'alpha', state.materials[i].alpha 
    # Reset camera parameters
    lookAt.set 'eye', constants.lookAt.eye
    lookAt.set 'look', constants.lookAt.look
    lookAt.set 'up', constants.lookAt.up
    # Change menu item
    ($ '#play-simulation a').text 'Simulate'
    ($ '#play-simulation').off().on 'click', playAnimation

###
  Update menus according to the current state
###
updateMenu = (state) ->
  switch state
    when 'logout'
      ($ '#menu-items').hide()
    when 'login'
      ($ '#menu-items').show()
    when 'model-empty'
      ($ '#save-bim-model').hide()
      ($ '#play-simulation').hide()
    when 'model-loaded'
      ($ '#save-bim-model').show()
      ($ '#play-simulation').show()

###
  Update the system variables which affects configuration
###
updatePreferences = (fps, alpha, slerp) ->
  constants.fps = parseInt(fps.val())
  constants.useAlpha = alpha.prop('checked')
  constants.useSlerp = slerp.prop('checked')

###
  Update the color used for hightlighting materials
###
updateHighlightColor = (hcolor) ->
  color = parseInt hcolor.val(), 16
  constants.highlightMaterial.baseColor =
    r: ((color & 0xff0000) >> 16) / 255.0
    g: ((color & 0x00ff00) >> 8) / 255.0
    b: (color & 0x0000ff) / 255.0
  if state.scene?
    library = state.scene.node 0
    (library.node library.numNodes() - 1).set 'baseColor', constants.highlightMaterial.baseColor

###
  Update the render configuration parameters
###
updateRender = (culling, transparency) ->
  constants.culling = culling.prop('checked')
  constants.transparency = transparency.prop('checked')
  flags = state.scene?.findNode('scene-flags').set 'flags',
    backfaces: constants.culling
    transparent: constants.transparency