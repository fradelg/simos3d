mouseDown = (event) ->
  return unless state.scene
  
  coords = getClientXY(event)
  state.viewport.mouse.lastX = coords[0]
  state.viewport.mouse.lastY = coords[1]
  
  # Activate the appropriate mouse button mode
  switch coords[2]
    when 1
      state.viewport.mouse.leftDown = true
      # Pick the object under the mouse on left click
      state.viewport.mouse.pickRecord = state.scene.pick coords[0], coords[1]
      console.log 'Picked: ' + state.viewport.mouse.pickRecord?.name if state.debug
    when 2
      state.viewport.mouse.middleDown = true

  # Do not propagate the event
  return false

mouseUp = (event) ->
  return unless state.scene

  coords = getClientXY(event)
  
  switch coords[2]
    when 1
      # Select the picked object
      if state.viewport.mouse.leftDragDistance < constants.mouse.pickDragThreshold
        if state.viewport.mouse.pickRecord?
          controlsTreeSelectObject state.viewport.mouse.pickRecord.name
        state.viewport.mouse.pickRecord = null
      state.viewport.mouse.leftDown = false
      state.viewport.mouse.leftDragDistance = 0
    when 2
      state.viewport.mouse.middleDown = false
      state.viewport.mouse.middleDragDistance = 0

mouseMove = (event) ->
  return unless state.scene

  coords = getClientXY(event)
  
  # Get the delta position of the mouse over this frame
  delta = [coords[0] - state.viewport.mouse.lastX, coords[1] - state.viewport.mouse.lastY]
  deltaLength = SceneJS_math_lenVec2 delta
  return unless deltaLength > 0.0
  
  # Activate the appropriate mouse dragging mode
  state.viewport.mouse.leftDragDistance += deltaLength if state.viewport.mouse.leftDown
  state.viewport.mouse.middleDragDistance += deltaLength if state.viewport.mouse.middleDown
  
  if coords[2] is 1
    rotation = state.scene.findNode 'rotation'
    rotation.add 'rotation',
      x: 1.0
      y: 0.0
      z: 0.0
      angle: delta[1] * constants.camera.orbitSpeedFactor
    rotation.add 'rotation',
      x: 0.0
      y: 1.0
      z: 0.0
      angle: delta[0] * constants.camera.orbitSpeedFactor
  else if coords[2] is 2
    panVector = [-delta[0] * constants.camera.panSpeedFactor / deltaLength, delta[1] * constants.camera.panSpeedFactor / deltaLength]
    node = state.scene.findNode 'main-lookAt'
    lookAtNodePanRelative node, panVector

    # implement zoom with pitch gesture
    if event.handleObj.type.startsWith 'touchmove'
      touches = event.originalEvent.touches
      dx = touches[0].pageX - touches[1].pageX
      dy = touches[0].pageY - touches[1].pageY
      dst = Math.sqrt dx * dx + dy * dy
      state.zoom = 0.1 * constants.camera.zoomSpeedFactor * dst
      node = state.scene.findNode 'zoom-scale'
      node.set 'x', state.zoom
      node.set 'y', state.zoom
      node.set 'z', state.zoom
  
  # Update mouse positions
  state.viewport.mouse.lastX = coords[0]
  state.viewport.mouse.lastY = coords[1]

# Retrieve coordinates and button for mouse and touch events
getClientXY = (event) ->
  if event.handleObj.type.startsWith 'touch'
    touches = event.originalEvent.touches
    which = touches.length
    if which is 0
      which += 1
      clientX = 0
      clientY = 0
    else if which is 1
      clientX = touches[0].pageX
      clientY = touches[0].pageY
    else if which is 2
      clientX = 0.5 * (touches[0].pageX + touches[1].pageX)
      clientY = 0.5 * (touches[0].pageY + touches[1].pageY)
  else
    coords = mouseCoordsWithinElement event
    clientX = coords[0]
    clientY = coords[1]
    which = event.which
  return [clientX, clientY, which]

# Retrive click coordinates on a mouse event
mouseCoordsWithinElement = (event) ->
  coords = [0, 0]
  if not event
    event = window.event
    coords = [event.x, event.y]
  else
    element = event.target
    totalOffsetLeft = 0
    totalOffsetTop = 0
    while element.offsetParent
      totalOffsetLeft += element.offsetLeft
      totalOffsetTop += element.offsetTop
      element = element.offsetParent
    coords = [event.pageX - totalOffsetLeft, event.pageY - totalOffsetTop]
  return coords

projectToTrackball = (x, y) ->
  kUnitSphereRadius2D = Math.sqrt 2.0
  radius = constants.camera.kTrackBallRadius
  dist = Math.sqrt(x * x + y * y)

  if dist < (radius * kUnitSphereRadius2D / 2.0)
    z = Math.sqrt(radius * radius - dist * dist)
  else
    t = radius / kUnitSphereRadius2D
    z = t * t / dist
  if x < 0.0 || y < 0.0
    z *= -1.0

mouseWheel = (event, delta, deltaX, deltaY) ->
  return unless state.scene
  state.zoom *= 1.0 + delta * constants.camera.zoomSpeedFactor
  node = state.scene.findNode 'zoom-scale'
  node.set 'x', state.zoom
  node.set 'y', state.zoom
  node.set 'z', state.zoom
  
# Pan camera relative to screen space for manipulating the SceneJS lookat node
lookAtNodePanRelative = (lookAt, dPosition) ->
  if dPosition[0] == 0.0 and dPosition[1] == 0.0
    return { eye: (lookAt.get 'eye'), look: (lookAt.get 'look'), up: (lookAt.get 'up') }

  eye = recordToVec3 (lookAt.get 'eye')
  look = recordToVec3 (lookAt.get 'look')
  up = recordToVec3 (lookAt.get 'up')

  # Calculate the view axes
  axes = [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]]
  SceneJS_math_subVec3 eye, look, axes[2]
  SceneJS_math_cross3Vec3 up, axes[2], axes[0]
  SceneJS_math_normalizeVec3 axes[0]
  SceneJS_math_cross3Vec3 axes[2], axes[0], axes[1]
  SceneJS_math_normalizeVec3 axes[1]

  # Project dPosition from screen space into world space
  SceneJS_math_mulVec3Scalar axes[0], dPosition[0]
  SceneJS_math_mulVec3Scalar axes[1], dPosition[1]
  dPositionProj = [0.0, 0.0, 0.0]
  SceneJS_math_addVec3 axes[0], axes[1], dPositionProj
  lookAt.set 'eye', vec3ToRecord SceneJS_math_addVec3 eye, dPositionProj
  lookAt.set 'look', vec3ToRecord SceneJS_math_addVec3 look, dPositionProj
  
mainmenuViewsReset = (event) ->
  if state.scene?
    lookAtNode = state.scene.findNode 'main-lookAt'
    lookAtNode.set 'eye', state.lookAt.defaultParameters.eye
    lookAtNode.set 'look', state.lookAt.defaultParameters.look
    lookAtNode.set 'up', state.lookAt.defaultParameters.up

