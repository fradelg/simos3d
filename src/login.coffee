updateTips = (t) ->
  tips = $ '.validateTips'
  (tips.text t).addClass "ui-state-highlight"
  setTimeout () ->
    tips.removeClass "ui-state-highlight", 1500
  , 500

checkLength = (o, n, min, max) ->
  if o.val().length > max or o.val().length < min
    o.addClass 'ui-state-error'
    updateTips 'Length of ' + n + ' must be between ' + min + ' and ' + max + '.'
    return false
  else
    return true

checkRegexp = (o, regexp, n) ->
  if not regexp.test o.val()
    o.addClass 'ui-state-error'
    updateTips n
    return false
  else
    return true

###
  Log out the current user from the web application
###
logout = () ->
  $.cookie 'the_cookie', null, { expires: 7, path: '/' }
  # Tooggle the logout menu item text and handler
  ($ '#menu-items').hide()
  ($ '#login-button a').text 'Login'
  ($ '#login-button').off().on 'click', showLogin
  # Cleanup the scene and viewport
  cleanCanvas()
  viewportInit()
  ($ '#main-view-controls').hide()

showLogin = () ->
  ($ '#dialog-login').dialog 'open'

###
  Login into the system with an e-mail and password
###
login = (email, password) ->
  # Validate inputs
  bValid = true
  ($ '#dialog-login input').removeClass 'ui-state-error'
  bValid = bValid && checkLength email, 'email', 6, 80
  bValid = bValid && checkLength password, 'password', 5, 16
  bValid = bValid && checkRegexp email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com"
  bValid = bValid && checkRegexp password, /^([0-9a-zA-Z])+$/, 'Password field only allow : a-z 0-9'
  return false if not bValid

  # Extract input values
  user = email.val()
  pwd = hex_md5 password.val()

  # Call the REST API
  $.ajax
    url: constants.srvUrl + 'login/' + (encodeURIComponent user) + '/' + (encodeURIComponent pwd)
    success: (data, textStatus, jqXHR) -> 
      console?.log? 'Login request succeeded'
      # Set a session cookie valid for seven days
      if data.success
        $.cookie 'the_cookie', user, { expires: 7, path: '/' } 
        updateMenu 'login'
        updateMenu 'model-empty'
        ($ '#login-button a').text 'Logout'
        ($ '#login-button').off().on 'click', logout
        return true
    error: (jqXHR, textStatus, errorThrown) -> 
      console?.log? 'Login request failed with password ' + pwd
      return false
  
###
  Register the user into the system
###
registerNewUser = (name, email, password) ->
  ($ '#dialog-newuser input').removeClass 'ui-state-error'
  bValid = true
  bValid = bValid && checkLength name, "name", 3, 16
  bValid = bValid && checkLength email, "email", 6, 80
  bValid = bValid && checkLength password, "password", 5, 16
  bValid = bValid && checkRegexp name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter."
  bValid = bValid && checkRegexp email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com"
  bValid = bValid && checkRegexp password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9"
  return false if not bValid
  
  name = name.val()
  user = email.val()
  pwd = hex_md5 password.val()

  # Call the REST API
  $.ajax
    url: constants.srvUrl + 'register/' + (encodeURIComponent name) + 
      '/' + (encodeURIComponent user) + '/' + (encodeURIComponent pwd)
    success: (data, textStatus, jqXHR) -> 
      console?.log? 'User registration was successful'
      return data.success
    error: (jqXHR, textStatus, errorThrown) -> 
      console?.log? 'Registration action has failed'
      return false

###
  Send an e-mail with the user password
###
rememberPassword = (email) ->
  bValid = true
  email.removeClass "ui-state-error"
  bValid = bValid && checkLength email, "email", 6, 80
  bValid = bValid && checkRegexp email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com"
  return false if not bValid
  
  # Call the REST API
  $.ajax
    url: constants.srvUrl + 'remember/' + (encodeURIComponent email.val())
    success: (data, textStatus, jqXHR) -> 
      console?.log? 'An e-mail with your password has been sent'
      return data.success
    error: (jqXHR, textStatus, errorThrown) -> 
      console?.log? 'The e-mail with your password cannot be sent'
      return false