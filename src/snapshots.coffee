snapshotsPush = () ->
  return if not state.scene?  

  # Generate the thumbnail of the snapshot
  thumbSize = constants.thumbnails.size  
  imgURI = canvasCaptureThumbnail state.canvas, 512 * thumbSize[0] / thumbSize[1], 512, constants.thumbnails.scale * thumbSize[0], constants.thumbnails.scale * thumbSize[1]
  ($ '#snapshots').append "<div class='snapshot'><div class='snapshot-thumb'>
    <a href='#' class='snapshot-delete'>x</a><img width='" + thumbSize[0] + "px' height='" + 
    thumbSize[1] + "px' src='" + imgURI + "'></div></div>"

  node = state.scene.findNode 'main-lookAt'
  state.snapshots.lookAts.push
    eye: node.get 'eye'
    look: node.get 'look'
    up: node.get 'up'

snapshotsDelete = (event) ->
  $parent = ($ event.target).parent()
  state.snapshots.lookAts.splice $parent.index() + 1, 1
  $parent.remove()

# Show a toggled snapshot in the main view
snapshotsToggle = (event) ->
  return if not state.scene?  
  # TODO: SceneJS.FX.transition (state.scene.findNode 'main-lookAt'), state.snapshots, { interpolation: 'linear' }

snapshotsPlay = (event) ->
  return if not state.scene?  
  node = state.scene.findNode 'main-lookAt'
  (SceneJS.FX.TweenSpline node).sequence state.snapshots.lookAts