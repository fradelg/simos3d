
$ () ->
  # Initialize state variables
  state.canvas = document.getElementById 'scenejsCanvas'
  state.viewport.domElement = document.getElementById 'viewport'
  state.canvas.width = $('#viewport').width()
  state.canvas.height = $('#viewport').height()

  viewportInit()
  registerDOMEvents()
  registerControlEvents()
  state.application.initialized = true

  SceneJS.setConfigs
    pluginPath: "/lib/scenejs/plugins"

  # Create jQuery UI dialogs
  $('#dialog-binding').dialog
    autoOpen: false
    width: 940
    height: 540
    buttons:
      "Next": () ->
        $(this).dialog 'close'
        controlsInit()

  $('#dialog-import').dialog
    autoOpen: false
    width: 505
    height: 290
    modal: true
    open: () ->
      # Init the main HTML elements of the dialog
      $('#box-3dmodel').removeClass()
      $('#box-3dmodel').addClass 'dropbox'
      $('#box-3dmodel .message').show()
      $('#box-schedule').removeClass()
      $('#box-schedule').addClass 'dropbox'
      $('#box-schedule .message').show()

  $('#dialog-login').dialog
    autoOpen: false
    height: 375
    width: 400
    modal: true
    open: () ->
      # Bind the enter key event to the default button in all dialogs
      $('.ui-dialog-buttonpane').find('button:first').focus()
      $('#dialog-login').keypress (e) ->
        if e.keyCode == $.ui.keyCode.ENTER
          $('#dialog-login').siblings('.ui-dialog-buttonpane').find('button:first').trigger 'click'
    buttons:
      Login: () ->
        email = $("#login input[name=email]")
        paswd = $("#login input[name=password]")
        if login(email, paswd)
          $(this).dialog('close')
      Cancel: () ->
        $(this).dialog('close')

  $('#dialog-newuser').dialog
    autoOpen: false
    height: 400
    width: 350
    modal: true
    buttons:
      'Register': () ->
        name = $("#login input[name=username]")
        email = $("#login input[name=email]")
        paswd = $("#login input[name=password]")
        if registerNewUser(username, email, paswd)
          $(this).dialog 'close'
      Cancel: () -> $(this).dialog 'close'

  $('#dialog-remember').dialog
    autoOpen: false
    height: 300
    width: 350
    modal: true
    buttons:
      'Remember': () ->
        if rememberPassword($('input[name=email_remember]'))
          $(this).dialog('close')
      Cancel: () ->
        $(this).dialog('close')

  $('#dialog-preferences').dialog
    autoOpen: false
    width: 400
    height: 400
    buttons:
      'Save': () ->
        updateHighlightColor($('input[name=hcolor]'))
        updatePreferences($('input[name=fps]'), $('input[name=alpha]'), $('input[name=slerp]'))
        updateRender($('input[name=culling]'), $('input[name=transparency]'))
        $(this).dialog('close')
      Cancel: () ->
        $(this).dialog('close')

  $('#dialog-load').dialog
    autoOpen: false
    height: 400
    width: 800
    modal: true
    buttons:
      'Load': () ->
        selected = $ '#bim-models .bimmodel-selected'
        return if selected.length == 0
        loadBimFromServer selected.attr 'id'
        $(this).dialog 'close'
      'Modify': () ->
        selected = $ '#bim-models .bimmodel-selected'
        return if selected.length == 0
        state.files.model = selected.attr 'id'
        state.files.updated = true
        $(this).dialog 'close'
        $('#dialog-save').dialog 'open'
      'Delete': () ->
        if confirm 'Are you sure you want to delete this model?'
          selected = $ '#bim-models .bimmodel-selected'
          return if selected.length == 0
          removeBimFromServer selected.attr 'id'
          $(this).dialog 'close'
          loadBimModel()
      Cancel: () ->
        $(this).dialog 'close'

  $('#dialog-save').dialog
    autoOpen: false
    height: 300
    width: 340
    modal: true
    open: () ->
      if state.files.updated
        cells = $('#bim-models .bimmodel-selected').children 'td'
        return if cells.length == 0
        $('#dialog-save #filename').val cells.eq(0).html()
        $('#dialog-save #description').val cells.eq(3).html()
      else
        $('#dialog-save #filename').val 'building'
        $('#dialog-save #description').val 'A 3D model of '
    buttons:
      'Save': () ->
        valid = filename.value.length > 0
        valid = valid & description.value.length > 0
        if valid
          saveBimModel filename.value, description.value, state.files.updated
          state.files.updated = false
          $(this).dialog 'close'
      Cancel: () ->
        $(this).dialog 'close'

  # Check if the cookie exists -> the user is already logged
  if $.cookie('the_cookie')?
    $('#login-button a').text 'Logout'
    $('#login-button').click logout
    updateMenu 'model-empty'
  else
    updateMenu 'logout'