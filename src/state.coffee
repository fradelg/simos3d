###
  The program state
  No global state is allowed to exist outside of this structure
###
state =
  scene: do () ->
    try
      return SceneJS.scene 'Scene'
    catch error
      return null
  canvas: null
  debug: false
  settings:
    picking: false
    performance: 'quality'
    mode: 'basic'
    light: true
  viewport:
    domElement: null
    selectedIfcObject: null
    mouse:
      lastX: 0
      lastY: 0
      leftDown: false
      middleDown: false
      leftDragDistance: 0
      middleDragDistance: 0
      pickRecord: null
  camera:
    distanceLimits: [0.0, 0.0]
  translation:
    x: 0.0
    y: 0.0
  zoom: 1.0
  parentActivity: null
  snapshots:
    lookAts: []
  application:
    initialized: false
  materials: []
  binding: []
  files:
    updated: false
    model: ''
    mpp: ''

# Application constants
constants =
  srvUrl: 'srv/'
  useAlpha: true
  useSlerp: true
  fps: 25
  lookAt:
    look: { x: 0.0, y: 0.2, z: 1.0 }
    eye: { x: 0.0, y: 0.2, z: 0.0 }
    up: { x: 0.0, y: 1.0, z: 0.0 }
  camera:
    kTrackBallRadius: 0.8
    maxOrbitSpeed: Math.PI * 0.1
    orbitSpeedFactor: 0.15
    zoomSpeedFactor: 0.05
    panSpeedFactor: 0.005
  mouse:
    # Distance that the mouse can be dragged while picking
    pickDragThreshold: 10
  canvas:
    defaultSize: [1024, 512]
    topOffset: 122
  thumbnails:
    size: [125, 100]
    scale: 2
  highlightMaterial:
    type: 'material'
    coreId: 'highlight'
    baseColor:
      r: 1.0
      g: 0.0
      b: 0.0
  sceneLight:
    type: "light"
    id: "main-light"
    mode: "point"
    color: { r: 1.0, g: 1.0, b: 1.0 }
    pos: { x: 1.0, y: 1.0, z: 1.0 }
    constantAttenuation: 1.0
    linearAttenuation: 0.0
    quadraticAttenuation: 0.0
