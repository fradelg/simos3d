# Events launched when a key is pressed
keyDown = (event) ->
  switch event.which
    when 72 # h - Toggle help
      topmenuHelp()
    when 76 # l - Toggle lighting
      state.lighting = not state.lighting
      camera = state.scene.findNode 'main-camera'
      if state.lighting
        (camera.node 0).splice()
      else
        camera.insert 'node', constants.sceneLight
