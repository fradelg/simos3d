# 3D SIMOS

3D SIMOS is a Web3D application to showcase a building project with its corresponding constructive stages.

## Dependencies

Here it is the list with the dependencies and their corresponding licenses:

- [SceneJS](http://scenejs.org) - MIT
- [jQuery-ui](https://jqueryui.com) - MIT
- [jquery File Upload](https://github.com/blueimp/jQuery-File-Upload) - MIT
- [jscolor](http://jscolor.com/) - GPLv3
- [jshash](http://anmar.eu.org/projects/jssha2) - BSD
- [uuid.js](https://github.com/broofa/node-uuid) - MIT

- [SQLite](https://www.sqlite.org/index.html) - Public domain
- [pyCollada](https://github.com/pycollada/pycollada) - BSD
- [MPX](http://www.mpxj.org/) - LGPL
- [Bottle](https://bottlepy.org/docs/dev/) - MIT
- [Py4J](https://www.py4j.org) - BSD

## Build

The application consists of two parts: the client side and the server side. The first part is a web application based on WebGL that helps users to bind geometries and plannings and render a 3D building. It can be compiled with:

´´´
npm install
npm run build
´´´

and then open `index.html` file in preferred your web browser.

The server code must be deployed through a web server running Python and Java. Dependencies on this side must be installed manually due to the heterogenity of technologies employed. However, the REST service can be started in the background with a single command run in the `srv` folder:

´´´
nohup python server.py &
´´´